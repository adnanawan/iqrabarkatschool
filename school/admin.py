# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin
from .models import (
    Student, Teacher, TeacherAttendance, StudentAttendance, Class, Course
)

admin.site.register(Student)
admin.site.register(Teacher)
admin.site.register(StudentAttendance)
admin.site.register(TeacherAttendance)
admin.site.register(Class)
admin.site.register(Course)
