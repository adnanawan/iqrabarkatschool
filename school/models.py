# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models


class Person(models.Model):
    first_name = models.CharField(max_length=100)
    last_name = models.CharField(max_length=100)
    dob = models.DateField('Date of birth')
    mobile = models.CharField(max_length=50)
    address = models.CharField(max_length=500)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        abstract = True
        ordering = ['first_name', 'last_name']

    def __str__(self):
        return '{} {}'.format(self.first_name, self.last_name)


class Parents(Person):
    occupation = models.CharField(max_length=100)
    email = models.EmailField(max_length=254, blank=True)
    annual_income = models.FloatField()


class Course(models.Model):
    name = models.CharField(max_length=50)
    description = models.CharField(max_length=200)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.name


class Teacher(Person):
    salary = models.FloatField()
    course = models.ManyToManyField(Course)


class Class(models.Model):
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    classes_list = [
        ('pre_nursery', 'Pre-Nursery (KG-1)'),
        ('nursery', 'Nursery (KG-2)'),
        ('prep', 'Preparatory (KG-3)'),
        ('1', 'Class One (1)'),
        ('2', 'Class Two (2)'),
        ('3', 'Class Three (3)'),
        ('4', 'Class Four (4)'),
        ('5', 'Class Five (5)'),
        ('6', 'Class Six (6)'),
        ('7', 'Class Seven (7)'),
        ('8', 'Class Eight (8)'),
        ('9', 'Class Nine (9)'),
        ('10', 'Class Ten (10)'),
    ]
    sections = [
        ('a', 'Section A'),
        ('b', 'Section B'),
        ('c', 'Section C'),
        ('d', 'Section D'),
        ('e', 'Section E'),
        ('f', 'Section F'),
    ]
    class_name = models.CharField(max_length=20, choices=classes_list, default='1')
    section = models.CharField(max_length=50, default='a', choices=sections)
    teacher = models.OneToOneField(Teacher, on_delete=models.CASCADE)

    def __str__(self):
        return '{} {}'.format(self.class_name, dict(self.sections).get(self.section))


class Student(Person):
    roll_no = models.CharField(max_length=15)
    course = models.ManyToManyField(Course)
    _class = models.ForeignKey(Class, on_delete=models.CASCADE)

    def __str__(self):
        return '{} {}-{}'.format(self.roll_no, self.first_name, self.last_name)


class Attendance(models.Model):
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    status = models.BooleanField()
    remarks = models.CharField(max_length=100)
    date_time_format = '%a, %d-%m-%y %H:%M %p'

    class Meta:
        abstract = True
        ordering = ['updated_at']


class StudentAttendance(Attendance):
    student = models.ForeignKey(Student, on_delete=models.CASCADE)

    def __str__(self):
        if self.status:
            return '{} {} arrived at {}'.format(
                self.student.first_name,
                self.student.last_name,
                self.updated_at.strftime(self.date_time_format)
            )

        return '{} {} is absent at {}'.format(
            self.student.first_name,
            self.student.last_name,
            self.updated_at.strftime(self.date_time_format)
        )


class TeacherAttendance(Attendance):
    teacher = models.ForeignKey(Teacher, on_delete=models.CASCADE)

    def __str__(self):
        if self.status:
            return '{} {} arrived at {}'.format(
                self.teacher.first_name,
                self.teacher.last_name,
                self.updated_at.strftime(self.date_time_format)
            )

        return '{} {} is absent at {}'.format(
            self.teacher.first_name,
            self.teacher.last_name,
            self.updated_at.strftime(self.date_time_format)
        )
